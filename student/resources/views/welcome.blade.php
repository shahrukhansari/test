<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
     rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
      crossorigin="anonymous">
    <title>CRUD</title>
</head>
<body>


    <div class="container mt-5">

    <div class="row">

<div class="col-sm-6">

<form action="{{url('/create')}}" method="POST" id="Form_id">


    @csrf

    <div class="mb-3">
        <label for="name" class="form-label">Name</label>
        <input type="text" class="form-control" id="name" name="name">
    </div>
    <div class="mb-3">
        <label for="city" class="form-label">City</label>
        <input type="text" class="form-control" id="city" name="city" >
    </div>
    <div class="mb-3">
        <label for="marks" class="form-label">Marks</label>
        <input type="text" class="form-control" id="marks" name="marks" >
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

</div>
<div class="col-sm-6">


    <table class="table table-hover">

<thead>
    <th scope="col">ID</th>
    <th scope="col">Name</th>
    <th scope="col">City</th>
    <th scope="col">Marks</th>
    <th scope="col">Action</th>

</thead>


<tbody>
@foreach ($students as $item)

<tr>
    <th>{{$item->id}}</th>
    <td>{{$item->name}}</td>
    <td>{{$item->city}}</td>
    <td>{{$item->marks}}</td>
     

    <td>
        <a href="{{url('/edit' , $item ->id)}}" class="btn btn-info btn-success">Edit</a>

        
        <a href="{{url('/delete' , $item ->id)}}" class="btn btn-info btn-danger">Delete</a>
    </td>
</tr>
@endforeach

  </tbody>

  </table>

      </div>

    </div>

</div>
    




</body>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<script>
     
	 $(document).ready(function(){


		
	$("#Form_id").validate({
		rules: {
		  'name': {
			required: true,
             
			
		  }, 
	  
		  'city': {
			required: true,
			
		  },
		  'marks': {
			required: true,
	  
		  
		  },
		
	  },
      messages: {
                    name: {
                        required: "Plz Enter your name**",
                        
                    },
                    city:{
                     
                       required:"plz Enter your City **"
                    },
                    marks:{
                        required : " plz Enter your marks **",
                    },

   
      }


		  });
	   
});

</script>

</html> 