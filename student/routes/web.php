<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\studentcontroller;

Route::get('/', [studentcontroller::class, "index"])->name('index');

Route::post('create', [studentcontroller::class, "create"])->name('create');


Route::get('/edit/{id}', [studentcontroller::class, "edit"])->name('edit');

Route::post('/update/{id}', [studentcontroller::class, "update"])->name('update');

Route::get('/delete/{id}', [studentcontroller::class, "destroy"])->name('edit');



 
?>